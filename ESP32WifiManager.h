#ifndef _ESP32_WIFI_MANAGER_H_
#define _ESP32_WIFI_MANAGER_H_

#include <WebServer.h>

#define DEBUG_LOG_EN 1
#if DEBUG_LOG_EN
    #define DEBUG_LOG(a) Serial.println(a)
#else
    #define DEBUG_LOG(a) 
#endif


class ESP32WifiManager
{
public:
    ESP32WifiManager(String ap_name, String ap_password,WebServer *webserver);
  
    void start_wifi_ap();
    void stop_ap_mode();
    void handler_loop();
    int println_log(void *str);
    boolean isIp(String str);
    String toStringIp(IPAddress ip);
    WebServer *server;
    int loop_async_handler();
    boolean handle_captive_portal();
    void handle_not_found();
    void handle_root();
    boolean handle_config_save();
    boolean load_saved_config();
    void clear_config();
    int auto_connect(int timeout_ms);


private:
     
    String ap_name;
    String ap_password;
    String ssid_name;
    String ssid_pass;
    String remote_host;
    String remote_username;
    String remote_password;
    String default_ap_name;//use incase of failed reload config failed
    String default_ap_pass;//use incase of failed reload config failed
    boolean save_config();


};

#endif