
#define DEFAULT_AP_NAME "RelayDeviceConfig"
#define DEFAULT_AP_PASS "12345678"
#define CONNECT_TO_CLIENT_TIMEOUT_MS 5000
#define TIMER_IRQ_PERIOD_MS 100
#define AP_PERIODE_MS 1 * 60 * 1000       /*5 min */
#define CHECK_WIFI_STATUS_PERIOD 5 * 1000 /*5 sec*/
#include "Arduino.h"
#include "ESP32WifiManager.h"

WebServer server(80);
ESP32WifiManager *myWifi;

//============IRQ flag=========================
volatile int irq_tick = 0;
volatile int ap_timeout = AP_PERIODE_MS;                 //if ap_timeout <0. ESP will be reset.
volatile int check_wifi_tick = CHECK_WIFI_STATUS_PERIOD; //if check_wifi_tick <0. ESP will check wifi.
hw_timer_t *timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

void IRAM_ATTR onTimer()
{
  portENTER_CRITICAL_ISR(&timerMux);
  irq_tick++;

  //for wifi check
  if (WiFi.getMode() == WIFI_STA)
  {
    check_wifi_tick = check_wifi_tick - TIMER_IRQ_PERIOD_MS;
  }
  //for timeout AP mode
  if (WiFi.getMode() == WIFI_AP)
  {
    ap_timeout = ap_timeout - TIMER_IRQ_PERIOD_MS;
  }

  portEXIT_CRITICAL_ISR(&timerMux);
}

void enable_timer_isr(int interval_ms)
{
  int interval = interval_ms * 1000;
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, interval, true);
  timerAlarmEnable(timer);
}
void disable_timer_irq(){
   timerAlarmDisable(timer);
}
void ap_timeout_reset_loop_check()
{
  if (WiFi.getMode() == WIFI_AP)
  {
    if (ap_timeout < 0)
    {
      myWifi->stop_ap_mode();
      delay(500);
      Serial.println("Ap mode Time out.Disconnect then start STA again");
      WiFi.begin();
      //myWifi->auto_connect(CONNECT_TO_CLIENT_TIMEOUT_MS);
      // ESP.restart();
      // while (1)
      //   ;
    }
  }
}
void start_count_down_reset(int init)
{
  ap_timeout = init;
}

void wifi_setup()
{
  myWifi = new ESP32WifiManager(DEFAULT_AP_NAME, DEFAULT_AP_PASS, &server);
  // -- Set up required URL handlers on the web server.
  server.on("/", []() { myWifi->handle_root(); });
  server.on("/save_config", []() { myWifi->handle_config_save(); });
  server.on("/reset", []() { ESP.restart(); });
  server.onNotFound([]() { myWifi->handle_not_found(); }); //lamda C++ function type on C11
 
  if (myWifi->auto_connect(CONNECT_TO_CLIENT_TIMEOUT_MS) == WL_CONNECTED)
  {
    Serial.println("Successfully connect wifi");
    Serial.println("IP : " + WiFi.localIP().toString());
  }
  else
  {
    Serial.println("Failed connect wifi");
    myWifi->start_wifi_ap(); //start AP automatically
    start_count_down_reset(AP_PERIODE_MS);
  }
}
void check_wifi_status_loop()
{
  if (WiFi.getMode() == WIFI_STA)
  {
    if (check_wifi_tick < 0)
    {
      check_wifi_tick = CHECK_WIFI_STATUS_PERIOD;
      int status = WiFi.status();
      if (status == WL_CONNECTED)
      {
        Serial.println("Status = Wifi Connected");
      }
      else
      {
        Serial.print("Try to reconnect status = ");
        Serial.println(status);
      }
    }
  }
}

// int read_pin_delay(int ms)
// {
//     pinMode(2,IN)
// }
void setup()
{

  Serial.begin(115200);
  Serial.println();
  Serial.println("Starting up...");
  enable_timer_isr(TIMER_IRQ_PERIOD_MS); //100ms
  wifi_setup();

}

void loop()
{
  myWifi->loop_async_handler();
  //ap_timeout_reset_loop_check();
  check_wifi_status_loop();
}