#include "Arduino.h"
#include <Preferences.h>
#include <WiFi.h>
#include <DNSServer.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include "webPortalPage.h"
#include "ESP32WifiManager.h"
//this must be less than 15 char
#define CONFIG_FILENAME "CfgParas"

Preferences preferences;
const byte DNS_PORT = 53;
IPAddress apIP(192, 168, 4, 1);
DNSServer dnsServer;

ESP32WifiManager::ESP32WifiManager(String ap_name, String ap_password, WebServer *webserver)
{
  this->default_ap_name = ap_name;
  this->default_ap_pass = ap_password;
  this->server = webserver;
}
/* Is this an IP? */
boolean ESP32WifiManager::isIp(String str)
{
  for (size_t i = 0; i < str.length(); i++)
  {
    int c = str.charAt(i);
    if (c != '.' && (c < '0' || c > '9'))
    {
      return false;
    }
  }
  return true;
}
/** IP to String? */
String ESP32WifiManager::toStringIp(IPAddress ip)
{
  String res = "";
  for (int i = 0; i < 3; i++)
  {
    res += String((ip >> (8 * i)) & 0xFF) + ".";
  }
  res += String(((ip >> 8 * 3)) & 0xFF);
  return res;
}

boolean ESP32WifiManager::handle_captive_portal()
{
  String host = this->server->hostHeader();
  DEBUG_LOG(host.c_str());
  if (!isIp(host))
  {
    DEBUG_LOG("Request redirected to captive portal");

    DEBUG_LOG("Request for ");
    DEBUG_LOG(host.c_str());
    DEBUG_LOG(" redirected to ");
    DEBUG_LOG(this->server->client().localIP());

    this->server->sendHeader("Location", String("http://") + toStringIp(server->client().localIP()), true);
    this->server->send(302, "text/plain", ""); // Empty content inhibits Content-length header so we have to close the socket ourselves.
    this->server->client().stop();             // Stop is needed because we sent no content length
    return true;
  }
  return false;
}

void ESP32WifiManager::handle_root()
{
  if (this->handle_captive_portal())
  { // If captive portal redirect instead of displaying the error page.
    return;
  }
  else
  {
    this->server->send(200, "text/html", web_portal);
  }
}

void ESP32WifiManager::handle_not_found()
{
  if (this->handle_captive_portal())
  { // If captive portal redirect instead of displaying the error page.
    return;
  }
  else
  {
    String message = "File Not Found\n\n";
    message += "URI: ";
    message += server->uri();
    message += "\nMethod: ";
    message += (server->method() == HTTP_GET) ? "GET" : "POST";
    message += "\nArguments: ";
    message += server->args();
    message += "\n";
    for (uint8_t i = 0; i < server->args(); i++)
    {
      message += " " + server->argName(i) + ": " + server->arg(i) + "\n";
    }
    this->server->send(404, "text/plain", message);
  }
}

boolean ESP32WifiManager::save_config()
{
  DEBUG_LOG("***************Save Config*************************");
  String ap_name = server->arg("txtDeviceName");
  DEBUG_LOG("recv ap_name : " + ap_name);
  if (ap_name.length() < 5)
    return false;

  String ap_pass = server->arg("txtApPassword");
  DEBUG_LOG("recv ap_pass : " + ap_pass);
  if (ap_pass.length() < 5)
    return false;

  String ssid_name = server->arg("txtWifiSSID");
  DEBUG_LOG("recv ssid_name : " + ssid_name);
  if (ssid_name.length() < 5)
    return false;

  String ssid_pass = server->arg("txtWifiPassword");
  DEBUG_LOG("recv ssid_pass : " + ssid_pass);
  if (ssid_pass.length() < 5)
    return false;

  String mqtt_host = server->arg("mqttServer");
  DEBUG_LOG("recv mqtt_host : " + mqtt_host);
  if (mqtt_host.length() < 5)
    return false;

  String mqtt_user = server->arg("mqttUser");
  DEBUG_LOG("recv mqtt_user : " + mqtt_user);
  if (mqtt_user.length() < 5)
    return false;

  String mqtt_pass = server->arg("mqttPass");
  DEBUG_LOG("recv mqtt_pass : " + mqtt_user);
  if (mqtt_pass.length() < 5)
    return false;
  //save to configFile
  // Open Preferences with my-app namespace. Each application module, library, etc
  // has to use a namespace name to prevent key name collisions. We will open storage in
  // RW-mode (second parameter has to be false).
  // Note: Namespace name is limited to 15 chars.
  preferences.begin(CONFIG_FILENAME, false);
  DEBUG_LOG("Save to config file");
  preferences.putString("ap_name", ap_name);
  preferences.putString("ap_pass", ap_pass);
  preferences.putString("ssid_name", ssid_name);
  preferences.putString("ssid_pass", ssid_pass);
  preferences.putString("mqtt_host", mqtt_host);
  preferences.putString("mqtt_user", mqtt_user);
  preferences.putString("mqtt_pass", mqtt_pass);
  preferences.end();

  return true;
}
boolean ESP32WifiManager::handle_config_save()
{
  // DEBUG_LOG('Uri ' +server->args());
  // DEBUG_LOG('Number of args : ' +server->uri());
  boolean ret = this->save_config();
  String s = "<!DOCTYPE html><html lang=\"en\"><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"/>";
  s += "<title>Relay device Config</title></head><body>";
  if (ret)
  {
    s += "Config save OK.Go to <a href='reset'>Reset Device</a> to apply new value.";
    s += "</body></html>\n";
  }
  else
  {
    s += "Config save failed.Go to <a href='/'>Go to homepage</a> setup page again.";
    s += "</body></html>\n";
  }
  this->server->send(200, "text/html", s);
  return ret;
}

boolean ESP32WifiManager::load_saved_config()
{
  // Open Preferences with my-app namespace. Each application module, library, etc
  // has to use a namespace name to prevent key name collisions. We will open storage in
  // RW-mode (second parameter has to be false).
  // Note: Namespace name is limited to 15 chars.
  preferences.begin(CONFIG_FILENAME, false);

  DEBUG_LOG("Load config from config file");
  this->ap_name = preferences.getString("ap_name", "");
  this->ap_password = preferences.getString("ap_pass", "");
  this->ssid_name = preferences.getString("ssid_name", "");
  this->ssid_pass = preferences.getString("ssid_pass", "");
  this->remote_host = preferences.getString("mqtt_host", "");
  this->remote_username = preferences.getString("mqtt_user", "");
  this->remote_password = preferences.getString("mqtt_pass", "");
  preferences.end();

  DEBUG_LOG("*************Load config from SPI flash**************");
  DEBUG_LOG("ap_name : " + this->ap_name);
  DEBUG_LOG("ap_password : " + this->ap_password);
  DEBUG_LOG("ssid_name : " + this->ssid_name);
  DEBUG_LOG("ssid_pass : " + this->ssid_pass);
  DEBUG_LOG("remote_host : " + this->remote_host);
  DEBUG_LOG("remote_username : " + this->remote_username);
  DEBUG_LOG("remote_password : " + this->remote_password);
  DEBUG_LOG("*****************************************************");

  if (this->ap_name.length() < 5)
    return false;
  if (this->ap_password.length() < 5)
    return false;
  if (this->ssid_name.length() < 5)
    return false;
  if (this->ssid_pass.length() < 5)
    return false;
  if (this->remote_host.length() < 5)
    return false;
  if (this->remote_username.length() < 5)
    return false;
  if (this->remote_password.length() < 5)
    return false;

  return true;
}

void ESP32WifiManager::clear_config()
{
  // Open Preferences with my-app namespace. Each application module, library, etc
  // has to use a namespace name to prevent key name collisions. We will open storage in
  // RW-mode (second parameter has to be false).
  // Note: Namespace name is limited to 15 chars.
  preferences.begin(CONFIG_FILENAME, false);
  preferences.clear();
  preferences.end();
}

int ESP32WifiManager::auto_connect(int timeout_ms)
{
  int status;
  DEBUG_LOG("STA Booting up...");
  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  this->stop_ap_mode();
  
  if (this->load_saved_config())
  {
    DEBUG_LOG("Loading the saved config OK");
    Serial.print("Connect to ");Serial.println( this->ssid_name);
    WiFi.begin(this->ssid_name.c_str(), this->ssid_pass.c_str());
    while (timeout_ms >= 0)
    {
      status =WiFi.status();
      if (status == WL_CONNECTED || status == WL_CONNECT_FAILED) {
        break;
      }
     //Serial.println("...");
      delay(200);
      timeout_ms = timeout_ms - 200;
    }
      return status;
  }
  else
  {
    return WL_CONNECT_FAILED;
  }
}

void ESP32WifiManager::stop_ap_mode(){
  WiFi.softAPdisconnect(true);
  WiFi.mode(WIFI_STA);
  delay(500); //to make sure to clear the wifi stack

}
void ESP32WifiManager::start_wifi_ap()
{
  DEBUG_LOG("AP Booting up...");
  WiFi.mode(WIFI_AP);
  delay(200);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));

  //load parameter to
  if (this->load_saved_config())
  {
    DEBUG_LOG("Loading the saved config");
    WiFi.softAP(this->ap_name.c_str(), this->ap_password.c_str());
  }
  else
  { //load failed
    WiFi.softAP(this->default_ap_name.c_str(), this->default_ap_pass.c_str());
    DEBUG_LOG("Loading the default config");
  }
  // if DNSServer is started with "*" for domain name, it will reply with
    dnsServer.setErrorReplyCode(DNSReplyCode::NoError);
  // provided IP to all DNS request
  dnsServer.start(DNS_PORT, "*", apIP);
  this->server->begin();
}
int ESP32WifiManager::loop_async_handler()
{
  if(WiFi.getMode()==WIFI_AP){
    dnsServer.processNextRequest();
    this->server->handleClient();
  }
}
