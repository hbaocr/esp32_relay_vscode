## Guide to setup
[link](https://medium.com/home-wireless/use-visual-studio-code-for-arduino-2d0cf4c1760b)
* Install plugin :
    * Install plugin `Visual Studio Code extension for ESP8266/ESP32 File System (SPIFFS)` named `ESP8266FS 1.1.0 ` from `kash4kev`
    * Install plugin `Arduino for Visual Studio Code` named `Arduino 0.2.25 ` from `Microsoft`
    * You need to install original `Arduino` IDE in default path ( as they guided in `ESP8266FS 1.1.0` plugin )
    * Using original Arduino IDE to install all boards (include install toolchain for esp32) and library 
* To kick start : `CMD + Shift + P ` on MAC to run `Vscode cmd` -->  chose `Arduino:board config` to choose the board you want to work with
* The config file in `./.vscode/c_cpp_properties.json` --> to help vscode can find the dependency to display and remind developer
* To choose the COM port to load : `./.vscode/arduino.json` --> modify to your COM port address

## Library
    * https://github.com/prampec/IotWebConf 
    * 